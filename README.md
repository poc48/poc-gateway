# Gateway
A gateway which owns GraphQL schema and ACL. Aggregates all service client jars.

* [Service](https://gitlab.com/poc48/poc-service)  - Spring Boot microservice.

# Gradle Dependency
* Client - `implementation 'com.poc.service:service-client:<version>`

# Local development
To download service dependencies update *gradle.properties* file in project root. Set you gitlab token.

Run if image in the gitlab registry: `docker-compose up`

Authorization token has to be passed as `Authorization Bearer {JWT}` header. Development token:
```
{
  "role": "DEVELOPER",
  "id": "f25691ff827c685f775c40b0063fcf45662b3856",
  "jti": "f25691ff827c685f775c40b0063fcf45662b3856",
  "iss": "",
  "aud": "poc",
  "sub": "5500",
  "exp": 2640995200,
  "iat": 1609317484,
  "token_type": "bearer",
  "scope": null
}
```

## Service image
* `docker pull registry.gitlab.com/poc48/poc-service/service-app:1.1`

## Generate SSH keys
```
ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
openssl pkcs8 -topk8 -in jwtRS256.key -inform pem -out jwtPKCS8.pem -outform pem -nocrypt
```

## Actuator
* `/actuator/health/liveness`
* `/actuator/health/readiness`

# Checkstyle
* Install [IDE plugin](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea).
* Add a custom styles by selecting the `config/checkstyle/checkstyle.xml`.

# SonarQube
* Go to https://sonarcloud.io/organizations/poc48/projects
