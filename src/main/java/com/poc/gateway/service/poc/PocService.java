package com.poc.gateway.service.poc;

import com.poc.common.api.ResponseDto;
import com.poc.service.api.service.BusinessDto;
import com.poc.service.api.service.CategoryDto;
import com.poc.service.api.service.DoBusinessDto;
import com.poc.service.client.service.ServiceFeignClient;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PocService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PocService.class);

    private final ServiceFeignClient serviceFeignClient;

    public ResponseDto doBusiness(DoBusinessDto request) {
        LOGGER.info("Do business: {}", request);
        return serviceFeignClient.doBusiness(request);
    }

    public List<CategoryDto> getAllCategories() {
        LOGGER.info("Service: get all categories.");
        return serviceFeignClient.getAllCategories();
    }

    public List<BusinessDto> getAllBusinesses() {
        LOGGER.info("Service: get all businesses.");
        return serviceFeignClient.getAllBusinesses();
    }
}
