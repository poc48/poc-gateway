package com.poc.gateway.graphql.instrumentation;

import graphql.ExecutionResult;
import graphql.execution.instrumentation.InstrumentationContext;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.SimpleInstrumentationContext;
import graphql.execution.instrumentation.parameters.InstrumentationExecutionParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RequestLoggingInstrumentation extends SimpleInstrumentation {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestLoggingInstrumentation.class);

    @Override
    public InstrumentationContext<ExecutionResult> beginExecution(InstrumentationExecutionParameters parameters) {
        MDC.put("requestId", UUID.randomUUID().toString());
        LOGGER.info("New query: {}", parameters.getQuery());
        return SimpleInstrumentationContext.whenCompleted((
                (executionResult, throwable) -> LOGGER.info("Query completed")));
    }
}
