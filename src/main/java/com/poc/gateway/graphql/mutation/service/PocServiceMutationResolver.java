package com.poc.gateway.graphql.mutation.service;

import com.poc.common.api.ResponseDto;
import com.poc.gateway.config.AuthorityConstants;
import com.poc.gateway.service.poc.PocService;
import com.poc.service.api.service.DoBusinessDto;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PocServiceMutationResolver implements GraphQLMutationResolver {
    private final PocService pocService;

    @PreAuthorize(AuthorityConstants.AUTHORIZED_GROUP)
    public ResponseDto doBusiness(DoBusinessDto dto) {
        return pocService.doBusiness(dto);
    }
}
