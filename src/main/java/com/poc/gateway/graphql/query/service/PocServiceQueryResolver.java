package com.poc.gateway.graphql.query.service;

import com.poc.gateway.config.AuthorityConstants;
import com.poc.gateway.service.poc.PocService;
import com.poc.service.api.service.BusinessDto;
import com.poc.service.api.service.CategoryDto;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PocServiceQueryResolver implements GraphQLQueryResolver {

    private final PocService pocService;

    @PreAuthorize(AuthorityConstants.AUTHORIZED_GROUP)
    public List<CategoryDto> getAllCategories() {
        return pocService.getAllCategories();
    }

    @PreAuthorize(AuthorityConstants.ALL_USERS)
    public List<BusinessDto> getAllBusinesses() {
        return pocService.getAllBusinesses();
    }
}
