package com.poc.gateway.security;

public enum Role {
    GUEST("GUEST"),
    DEVELOPER("DEVELOPER");

    private final String value;

    Role(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
