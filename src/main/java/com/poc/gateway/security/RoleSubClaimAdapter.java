package com.poc.gateway.security;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.oauth2.jwt.MappedJwtClaimSetConverter;

import java.util.Collections;
import java.util.Map;

public class RoleSubClaimAdapter implements Converter<Map<String, Object>, Map<String, Object>> {

    private final MappedJwtClaimSetConverter delegate = MappedJwtClaimSetConverter.withDefaults(Collections.emptyMap());

    public Map<String, Object> convert(Map<String, Object> claims) {
        Map<String, Object> convertedClaims = this.delegate.convert(claims);

        String claim = Role.GUEST.value();
        String role = (String) convertedClaims.get("role");
        if (role != null) {
            claim = Role.valueOf(role).value();
        }
        convertedClaims.put("role", claim);
        return convertedClaims;
    }
}
