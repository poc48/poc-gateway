package com.poc.gateway.config;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.poc.common.api.ServiceException;
import com.poc.common.api.ServiceValidationException;
import graphql.GraphQLError;
import graphql.GraphqlErrorBuilder;
import graphql.kickstart.execution.error.GraphQLErrorHandler;
import graphql.kickstart.servlet.apollo.ApolloScalars;
import graphql.kickstart.tools.SchemaParserOptions;
import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLScalarType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
public class GraphqlConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphqlConfiguration.class);

    @Bean
    public GraphQLScalarType date() {
        return ExtendedScalars.Date;
    }

    @Bean
    public GraphQLErrorHandler errorHandler() {
        return new GraphQLErrorHandler() {
            @Override
            public List<GraphQLError> processErrors(List<GraphQLError> errors) {
                return errors;
            }

            @ExceptionHandler(AccessDeniedException.class)
            public GraphQLError handle(AccessDeniedException e) {
                LOGGER.info("Access denied exception.", e);

                return GraphqlErrorBuilder.newError().message("error.access_denied").build();
            }

            @ExceptionHandler(ServiceException.class)
            public GraphQLError handleServiceException(ServiceException e) {
                LOGGER.info("Service exception.", e);

                return GraphqlErrorBuilder.newError()
                        .message(
                                e.getLabel() != null ? e.getLabel() : String.valueOf(e.getMessage())
                        )
                        .extensions(Map.of(
                                "message", String.valueOf(e.getMessage()),
                                "label", String.valueOf(e.getLabel()),
                                "status", String.valueOf(e.getStatus()),
                                "statusDescription", String.valueOf(e.getStatusDescription())
                        ))
                        .build();
            }

            @ExceptionHandler(ServiceValidationException.class)
            public List<GraphQLError> handleServiceValidationException(ServiceValidationException e) {
                LOGGER.info("Service validation exception.", e);

                return e.getValidationErrors().stream().map(ve -> GraphqlErrorBuilder.newError()
                        .message(String.valueOf(ve.getLabel()))
                        .path(Arrays.asList(
                                (Object[]) String.valueOf(ve.getLabel()).split("\\.")
                        ))
                        .extensions(Map.of(
                                "message", String.valueOf(ve.getMessage())
                        ))
                        .build()
                ).collect(Collectors.toList());
            }
        };
    }

    @Bean
    public SchemaParserOptions schemaParserOptions() {
        return SchemaParserOptions.newOptions().objectMapperConfigurer((mapper, context) -> {
            mapper.registerModule(new JavaTimeModule());
        }).build();
    }

    @Bean
    GraphQLScalarType uploadScalarType() {
        return ApolloScalars.Upload;
    }
}
