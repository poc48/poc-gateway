package com.poc.gateway.config;

public abstract class AuthorityConstants {
    public static final String ALL_USERS = "hasAnyAuthority("
            + "'ROLE_GUEST',"
            + "'ROLE_DEVELOPER'"
            + ")";
    public static final String AUTHORIZED_GROUP = "hasAnyAuthority("
            + "'ROLE_DEVELOPER'"
            + ")";

    private AuthorityConstants() {
    }
}
