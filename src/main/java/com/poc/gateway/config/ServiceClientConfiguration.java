package com.poc.gateway.config;

import com.poc.service.client.service.ServiceFeignClient;
import com.poc.service.client.service.ServiceFeignClientBuilder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "poc.service")
public class ServiceClientConfiguration {

    private String url;

    @Bean
    public ServiceFeignClient createPocServiceFeignClient() {
        return new ServiceFeignClientBuilder(url).build();
    }
}
