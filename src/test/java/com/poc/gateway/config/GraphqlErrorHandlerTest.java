package com.poc.gateway.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.graphql.spring.boot.test.GraphQLResponse;
import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.poc.common.api.ServiceError;
import com.poc.common.api.ServiceException;
import com.poc.common.api.ServiceValidationException;
import com.poc.gateway.graphql.AuthHelper;
import com.poc.gateway.security.Role;
import com.poc.service.client.service.ServiceFeignClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GraphqlErrorHandlerTest {
    @Autowired
    private GraphQLTestTemplate graphqlTestTemplate;
    @MockBean
    private ServiceFeignClient client;

    @Autowired
    private OAuth2ResourceServerProperties oAuth2ResourceServerProperties;

    @Test
    void testServiceExceptionHandling() throws Exception {
        ServiceError error = new ServiceError();
        error.setLabel("label");

        Mockito.when(client.getAllCategories()).thenThrow(new ServiceException(error));

        graphqlTestTemplate.setHeaders(generateAuthHeaders());
        GraphQLResponse response = graphqlTestTemplate.perform(
                "graphql/service/getAllCategories.graphqls",
                new ObjectMapper().createObjectNode()
        );

        assertTrue(response.getRawResponse().getBody().contains(error.getLabel()));
    }

    @Test
    void testHandledExceptionContainsValidationErrors() throws Exception {
        ServiceError validationError1 = new ServiceError(
                "poc.validation.label1",
                null
        );
        ServiceError validationError2 = new ServiceError(
                "poc.validation.label2",
                null
        );

        ServiceValidationException exception = new ServiceValidationException();
        exception.addValidationError(validationError1);
        exception.addValidationError(validationError2);

        Mockito.when(client.getAllCategories()).thenThrow(exception);

        graphqlTestTemplate.setHeaders(generateAuthHeaders());
        GraphQLResponse response = graphqlTestTemplate.perform(
                "graphql/service/getAllCategories.graphqls",
                new ObjectMapper().createObjectNode()
        );

        assertTrue(response.getRawResponse().getBody().contains(validationError1.getLabel()));
        assertTrue(response.getRawResponse().getBody().contains(validationError2.getLabel()));
    }

    private HttpHeaders generateAuthHeaders()
            throws InvalidKeySpecException, URISyntaxException, NoSuchAlgorithmException, JOSEException, IOException {
        JWTClaimsSet claims = new JWTClaimsSet.Builder().claim("role", Role.DEVELOPER.value()).build();
        String bearer = AuthHelper.jwtSigner(oAuth2ResourceServerProperties, claims).serialize();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + bearer);
        return new HttpHeaders(headers);
    }
}