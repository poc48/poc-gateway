package com.poc.gateway.graphql.mutation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.graphql.spring.boot.test.GraphQLResponse;
import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.poc.common.api.ResponseDto;
import com.poc.gateway.security.Role;
import com.poc.service.client.service.ServiceFeignClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.UUID;

import static com.poc.gateway.graphql.AuthHelper.generateAuthHeadersWithCompany;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PocServiceMutationResolverTest {
    @Autowired
    private GraphQLTestTemplate graphqlTestTemplate;

    @MockBean
    private ServiceFeignClient client;

    @Autowired
    private OAuth2ResourceServerProperties oAuth2ResourceServerProperties;

    @Test
    void doBusiness() throws Exception {
        UUID newBusinessId = UUID.randomUUID();

        ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", newBusinessId.toString());

        ResponseDto responseDto = new ResponseDto();
        responseDto.setResult(newBusinessId.toString());
        Mockito.when(client.doBusiness(any())).thenReturn(responseDto);

        graphqlTestTemplate.setHeaders(generateAuthHeadersWithCompany(
                Role.DEVELOPER,
                null,
                oAuth2ResourceServerProperties
        ));
        GraphQLResponse response = graphqlTestTemplate.perform(
                "graphql/service/doBusiness.graphqls",
                variables
        );

        assertTrue(response.isOk());

        String actual = response.get("$.data.doBusiness.result");
        assertEquals(newBusinessId.toString(), actual);
    }

    @Test
    void guestHasNoAccessToView() throws Exception {
        ObjectNode variables = new ObjectMapper().createObjectNode();
        variables.put("id", UUID.randomUUID().toString());

        Mockito.when(client.getAllCategories()).thenReturn(new ArrayList<>());

        graphqlTestTemplate.setHeaders(generateAuthHeadersWithCompany(
                Role.GUEST, null, oAuth2ResourceServerProperties
        ));
        GraphQLResponse response = graphqlTestTemplate.perform(
                "graphql/service/getAllCategories.graphqls",
                variables
        );

        assertTrue(response.getRawResponse().getBody().contains("error.access_denied"));
    }
}