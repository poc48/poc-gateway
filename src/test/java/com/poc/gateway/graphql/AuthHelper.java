package com.poc.gateway.graphql;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.poc.gateway.security.Role;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

public class AuthHelper {
    public static SignedJWT jwtSigner(
            OAuth2ResourceServerProperties properties,
            JWTClaimsSet claimsSet
    ) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, JOSEException, URISyntaxException {
        String privateKeyContent = new String(Files.readAllBytes(
                Paths.get(ClassLoader.getSystemResource("jwtPKCS8.pem").toURI())
        ));

        privateKeyContent = privateKeyContent
                .replaceAll("\\n", "")
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "");
        KeyFactory kf = KeyFactory.getInstance("RSA");

        PKCS8EncodedKeySpec keySpecPkcS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
        PrivateKey privateKey = kf.generatePrivate(keySpecPkcS8);

        JWSHeader header = new JWSHeader.Builder(
                JWSAlgorithm.parse(
                        properties.getJwt().getJwsAlgorithm()
                )
        )
                .contentType("text/plain")
                .customParam("exp", new Date().getTime())
                .build();

        SignedJWT jwsObject = new SignedJWT(header, claimsSet);

        RSASSASigner rsaSigner = new RSASSASigner(privateKey);
        jwsObject.sign(rsaSigner);

        return jwsObject;
    }

    public static HttpHeaders generateAuthHeaders(
            Role role, OAuth2ResourceServerProperties oAuth2ResourceServerProperties
    ) throws InvalidKeySpecException, URISyntaxException, NoSuchAlgorithmException, JOSEException, IOException {
        JWTClaimsSet claims = new JWTClaimsSet.Builder()
                .claim("role", role.value())
                .build();
        String bearer = AuthHelper.jwtSigner(oAuth2ResourceServerProperties, claims).serialize();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + bearer);
        return new HttpHeaders(headers);
    }

    public static HttpHeaders generateAuthHeadersWithCompany(
            Role role,
            Integer company,
            OAuth2ResourceServerProperties oAuth2ResourceServerProperties
    ) throws InvalidKeySpecException, URISyntaxException, NoSuchAlgorithmException, JOSEException, IOException {

        JWTClaimsSet claims = new JWTClaimsSet.Builder()
                .claim("role", role.value())
                .claim(
                        "account",
                        new HashMap<>() {{
                            put("company", company);
                        }}
                )
                .build();
        String bearer = AuthHelper.jwtSigner(oAuth2ResourceServerProperties, claims).serialize();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + bearer);
        return new HttpHeaders(headers);
    }
}
