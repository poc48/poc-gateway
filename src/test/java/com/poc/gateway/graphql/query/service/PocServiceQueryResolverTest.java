package com.poc.gateway.graphql.query.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.graphql.spring.boot.test.GraphQLResponse;
import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.poc.gateway.security.Role;
import com.poc.service.client.service.ServiceFeignClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

import static com.poc.gateway.graphql.AuthHelper.generateAuthHeaders;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PocServiceQueryResolverTest {
    @Autowired
    private GraphQLTestTemplate graphqlTestTemplate;

    @MockBean
    private ServiceFeignClient client;

    @Autowired
    private OAuth2ResourceServerProperties oAuth2ResourceServerProperties;

    @Test
    void getAllCategories() throws Exception {
        ObjectNode variables = new ObjectMapper().createObjectNode();

        Mockito.when(client.getAllCategories()).thenReturn(new ArrayList<>());

        graphqlTestTemplate.setHeaders(generateAuthHeaders(Role.DEVELOPER, oAuth2ResourceServerProperties));
        GraphQLResponse response = graphqlTestTemplate.perform(
                "graphql/service/getAllCategories.graphqls",
                variables
        );

        assertTrue(response.isOk());
    }

    @Test
    void getAllBusinesses() throws Exception {
        ObjectNode variables = new ObjectMapper().createObjectNode();

        Mockito.when(client.getAllBusinesses()).thenReturn(new ArrayList<>());

        graphqlTestTemplate.setHeaders(generateAuthHeaders(Role.DEVELOPER, oAuth2ResourceServerProperties));
        GraphQLResponse response = graphqlTestTemplate.perform(
                "graphql/service/getAllBusinesses.graphqls",
                variables
        );

        assertTrue(response.isOk());
    }
}